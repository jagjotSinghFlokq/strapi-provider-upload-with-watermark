# Add watermark for AWS S3 image uploader

## What does it do?

This uploader will add a watermark for every image you upload in the media library. Currently the watermark image is static and it can only be changed from the code.

## Usage

Just install the plugin and try uploading an image. It will add a watermark to it. To upload an image without the watermark, prefix the image name with `nw_`, ex. `nw_my_dog.jpeg`. It will skip adding the watermark to it.

## Installation

In your `config/plugins.js` file add the following config:

    module.exports = ({ env }) => ({
      upload: {
        provider: 'with-watermark',
        providerOptions: {
          accessKeyId: AWS_ACCESS_KEY_ID,
          secretAccessKey: AWS_ACCESS_SECRET,
          region: AWS_S3_REGION,
          params: {
            Bucket: AWS_S3_BUCKET,
          },
        },
      }
    });
    
You can also check `strapi-provider-upload-aws-s3` for more information about its params.
